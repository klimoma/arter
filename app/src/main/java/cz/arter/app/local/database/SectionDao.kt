package cz.arter.app.local.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import cz.arter.app.model.SectionEntity
import cz.arter.app.model.SectionType


@Dao
interface SectionDao {
    @Query("SELECT * FROM sectionentity")
    suspend fun getAll(): List<SectionEntity>

    @Query("SELECT * FROM sectionentity WHERE type LIKE :bigDashboadType OR type LIKE :smallDashboadType")
    suspend fun getDashboardSections(bigDashboadType: SectionType =SectionType.BIG_DASHBOARD_TILE, smallDashboadType: SectionType =SectionType.SMALL_DASHBOARD_TILE) : List<SectionEntity>

    @Query("SELECT * FROM sectionentity WHERE guid IN (:guids) ORDER BY sortIndex ASC LIMIT  3")
    suspend fun getSectionsByGuidsWithLimit(guids: List<String>) : List<SectionEntity>

    @Query("SELECT * FROM sectionentity WHERE guid IN (:guids) ORDER BY sortIndex")
    suspend fun getSectionsByGuids(guids: List<String>) : List<SectionEntity>

    @Query("SELECT * FROM sectionentity WHERE guid LIKE :guid")
    suspend fun getSectionsByGuid(guid: String) :SectionEntity

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(list: List<SectionEntity>)
}

