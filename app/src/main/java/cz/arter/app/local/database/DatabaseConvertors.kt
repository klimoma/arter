package cz.arter.app.local.database

import androidx.room.TypeConverter
import cz.arter.app.model.Widget
import kotlinx.serialization.json.Json
import kotlinx.serialization.*

class DatabaseConvertors {
    @TypeConverter
    fun widgetsFromJson(json: String?): List<Widget>? {
        return json?.let { Json.decodeFromString<List<Widget>>(it) }
    }

    @TypeConverter
    fun widgetsToJson(list: List<Widget>?): String? {
        return list?.let { Json.encodeToString(it)}
       }

    @TypeConverter
    fun stringsFromJson(json: String?): List<String>? {
        return json?.let { Json.decodeFromString<List<String>>(it) }
    }

    @TypeConverter
    fun stringsToJson(list: List<String>?): String? {
        return list?.let { Json.encodeToString(it)}
    }

}