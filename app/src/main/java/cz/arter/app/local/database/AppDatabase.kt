package cz.arter.app.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import cz.arter.app.model.SectionEntity

@TypeConverters(DatabaseConvertors::class)
@Database(entities = [SectionEntity::class], version = 5)
abstract class AppDatabase : RoomDatabase() {
    abstract fun sectionDao(): SectionDao
}
