package cz.arter.app.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.net.UrlQuerySanitizer
import android.webkit.MimeTypeMap
import java.net.URI


class BaseUtils {
    companion object{
        fun openFileUrl(url: String, context: Context) {
            val pdfIntent = Intent(Intent.ACTION_VIEW).apply {
                setDataAndType(Uri.parse(url), getMimeType(url))
            }
            context.startActivity(
            //    if(pdfIntent.resolveActivity(context.packageManager) == null) {
             //   browseURL(url, true)
           // } else
                Intent.createChooser(pdfIntent,"Vyberte si").apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK
                }
            )

        }

        fun getMimeType(url: String): String? {
            val extension = url.substring(url.lastIndexOf("."))
            val mimeTypeMap = MimeTypeMap.getFileExtensionFromUrl(extension)
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(mimeTypeMap)
        }
        fun parseUrl(url: String?): ParsedUrl? {
            if (url.isNullOrEmpty()) return null
            val uri = try {
                URI.create(url)
            } catch (e: IllegalArgumentException) {
                return null
            }
            val params =
                UrlQuerySanitizer().apply { allowUnregisteredParamaters = true; parseUrl(url) }
            return ParsedUrl(url, uri.scheme, uri.authority, params)
        }
        data class ParsedUrl(
            val orig: String,
            val scheme: String?,
            val authority: String?,
            val params: UrlQuerySanitizer
        )

    }
}