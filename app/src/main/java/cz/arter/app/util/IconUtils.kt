package cz.arter.app.util

import android.content.Context
import cz.arter.app.R

fun Context.getResourceDrawable(url: String): Int{
    val pUrl = BaseUtils.parseUrl(url)
    val name = pUrl?.authority
   if(pUrl?.scheme == "assets"){
       val resId = this.resources.getIdentifier(
            name,
            "drawable",
            this.packageName
        )
       return if(resId > 0) resId  else R.drawable.arter_default
    }
    return R.drawable.arter_default
}