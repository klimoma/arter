package cz.arter.app.util

import android.content.Context
import android.net.UrlQuerySanitizer
import androidx.compose.ui.platform.UriHandler
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.arter.app.ui.destinations.AlertDialogScreenDestination
import cz.arter.app.ui.destinations.GeneralScreenDestination
import java.net.URI



fun DestinationsNavigator.schemeNavigation( scheme: String, sectionGuid: String? = null, context: Context, uriHandler: UriHandler){
    val pUrl = BaseUtils.parseUrl(scheme)
    val urls = pUrl?.authority?.split("?")
    var showError = false
    when (pUrl?.scheme) {
       "app" -> when(pUrl.authority){
           "general" ->{
               sectionGuid?.let {
                   this.navigate(GeneralScreenDestination(it))  } ?: {showError = true}
           }
           "file" ->{
               val externalUrl =  pUrl.params.getValue("value")
                BaseUtils.openFileUrl(externalUrl, context)
           }
           else ->  showError = true
       }
        else -> {
            showError = true
        }
    }
    if(showError)
        this.navigate(AlertDialogScreenDestination("Chyba", "Tato funkce není dostupná."))
}