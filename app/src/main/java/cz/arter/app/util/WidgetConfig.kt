package cz.arter.app.util

object WidgetConfig {
    var regularFontSize: Int  = 14
    fun headerFontSize(size: Int) : Int{
        return 28 - 2 * size
    }
}