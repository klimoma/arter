package cz.arter.app.model

import cz.arter.app.util.EnumIgnoreUnknownSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Widget(
    @SerialName("height")
    var height: Int? = null,
    @SerialName("icon_url")
    var iconUrl: String? = null,
    @SerialName("images")
    var images: List<String>? = null,
    @SerialName("size")
    var size: Int? = null,
    @SerialName("src")
    var src: String? = null,
    @SerialName("subtitle")
    var subtitle: String? = null,
    @SerialName("text")
    var text: String? = null,
    @SerialName("type")
    var type: WidgetType? = null,
    @SerialName("url")
    var url: String? = null,
    @SerialName("width")
    var width: Int? = null
)


@Serializable(with = WidgetTypeEnumSerializer::class)
enum class WidgetType{
    @SerialName("link")
    LINK,
    @SerialName("header")
    HEADER,
    @SerialName("paragraph")
    PARAGRAPH,
    @SerialName("space")
    SPACE,
    @SerialName("image")
    IMAGE,
    @SerialName("gallery")
    GALLERY,
    UNKNOWN
}

object WidgetTypeEnumSerializer : EnumIgnoreUnknownSerializer<WidgetType>(WidgetType.values(), WidgetType.UNKNOWN)