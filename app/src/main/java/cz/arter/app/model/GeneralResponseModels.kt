package cz.arter.app.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GeneralResponseDto(
    @SerialName("created_at")
    var createdAt: String?,
    @SerialName("locale")
    var locale: String?,
    @SerialName("sections")
    var sections: List<SectionDto>?,
    @SerialName("version")
    var version: Int?
)