package cz.arter.app.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import cz.arter.app.util.EnumIgnoreUnknownSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.jetbrains.annotations.NotNull

@Entity
data class SectionEntity (
    var childs: List<String>?,
    @PrimaryKey
    @NotNull
    var guid: String,
    var imageUrl: String?,
    var parents: List<String>?,
    var schemeUrl: String?,
    var searchText: String?,
    var slug: String?,
    var sortIndex: Int?,
    var subtitle: String?,
    var title: String?,
    var type: SectionType?,
    var widgets: List<Widget>?
)


@Serializable
data class SectionDto(
    @SerialName("childs")
    var childs: List<String>?,
    @SerialName("guid")
    var guid: String,
    @SerialName("image_url")
    var imageUrl: String?,
    @SerialName("parents")
    var parents: List<String>?,
    @SerialName("scheme_url")
    var schemeUrl: String?,
    @SerialName("search_text")
    var searchText: String?,
    @SerialName("slug")
    var slug: String?,
    @SerialName("sort_index")
    var sortIndex: Int?,
    @SerialName("subtitle")
    var subtitle: String?,
    @SerialName("title")
    var title: String?,
    @SerialName("type")
    var type: SectionType?,
    @SerialName("widgets")
    var widgets: List<Widget>?,
//    @SerialName("options")
//    var options: Map<String, String>?
)

data class Section(
    var childs: List<String>? = null,
    var guid: String?= null,
    var imageUrl: String?= null,
    var parents: List<String>?= null,
    var schemeUrl: String?= null,
    var searchText: String?= null,
    var slug: String?= null,
    var sortIndex: Int?= null,
    var subtitle: String?= null,
    var title: String?= null,
    var type: SectionType?= null,
    var widgets: List<Widget>?= null,
    // var options: Map<String, String>?,
){

    var childSections: List<Section>? = null

}

@Serializable(with = SectionEnumSerializer::class)
enum class SectionType{
    @SerialName("big_dashboard_tile")
    BIG_DASHBOARD_TILE,
    @SerialName("small_dashboard_tile")
    SMALL_DASHBOARD_TILE,
    @SerialName("list_item")
    LIST_ITEM,
    UNKNOWN
}

object SectionEnumSerializer : EnumIgnoreUnknownSerializer<SectionType>(SectionType.values(), SectionType.UNKNOWN)



fun SectionDto.toSectionEntity(): SectionEntity {
    return SectionEntity(
        childs = childs,
        guid = guid,
        imageUrl = imageUrl,
        parents = parents,
        schemeUrl = schemeUrl,
        searchText = searchText,
        slug = slug,
        sortIndex = sortIndex,
        subtitle = subtitle,
        title = title,
        type = type,
        widgets = widgets
    )
}

fun SectionDto.toSection(): Section {
    return Section(
        childs = childs,
        guid = guid,
        imageUrl = imageUrl,
        parents = parents,
        schemeUrl = schemeUrl,
        searchText = searchText,
        slug = slug,
        sortIndex = sortIndex,
        subtitle = subtitle,
        title = title,
        type = type,
        widgets = widgets
//        ,
//        options = null
    )
}


fun SectionEntity.toSection(): Section {
    return Section(
        childs = childs,
        guid = guid,
        imageUrl = imageUrl,
        parents = parents,
        schemeUrl = schemeUrl,
        searchText = searchText,
        slug = slug,
        sortIndex = sortIndex,
        subtitle = subtitle,
        title = title,
        type = type,
        widgets = widgets
//        ,
//        options = null
    )
}