package cz.arter.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class ArterApplication @Inject constructor() : Application(){
//
//    @Inject
//   lateinit var networkPlugin: NetworkFlipperPlugin
//

    override fun onCreate() {
        super.onCreate()
        //SoLoader.init(this, false)
//
//        if (FlipperUtils.shouldEnableFlipper(this)) {
//            val client = AndroidFlipperClient.getInstance(this)
//            client.addPlugin(InspectorFlipperPlugin(this, DescriptorMapping.withDefaults()))
//            client.addPlugin(DatabasesFlipperPlugin(this))
//            client.addPlugin(networkPlugin)
//            client.start()
//        }
    }
}