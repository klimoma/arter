package cz.arter.app.ui

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import com.ramcosta.composedestinations.annotation.Destination


@Destination
@Composable
fun DetailScreen (viewModel: MainViewModel = hiltViewModel()) {
    val sections = viewModel.section.collectAsState().value

    LazyColumn(modifier = Modifier.fillMaxHeight()) {
        items(sections){
            Text(text = it.title
                ?:"")
        }


    }
//    sections?.forEach {
//        Text(text = it.title?:"")
//    }

}