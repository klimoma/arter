package cz.arter.app.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.arter.app.model.Section
import cz.arter.app.repositories.GeneralRepository
import cz.arter.app.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject


@HiltViewModel
class MainViewModel@Inject constructor(
    private val generalRepository: GeneralRepository
): ViewModel()  {
    val _loading = MutableStateFlow(true)
    val loading = _loading.asStateFlow()
    val _sections = MutableStateFlow(listOf<Section>())
    val section = _sections.asStateFlow()


    fun getSections() {

         generalRepository.getAllSections().onEach {
                    if(it is Resource.Loading){
                        _loading.value = true
                    }else{
                        _loading.value = false
                    }
                _sections.value = it.data?: listOf()
                }.launchIn(viewModelScope)


    }
    init {
        getSections()
    }

}
