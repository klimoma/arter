package cz.arter.app.ui.Dashboard

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.arter.app.model.Section
import cz.arter.app.repositories.GeneralRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject
@HiltViewModel
class DashboardViewModel @Inject constructor(
    private val generalRepository: GeneralRepository
): ViewModel()  {

    val _dashboardSection = MutableStateFlow(listOf<Section>())
    val dashboardSection = _dashboardSection.asStateFlow()

// }

    fun getSections() {
        generalRepository.getDashboardSection().onEach {
            _dashboardSection.value = it
        }.launchIn(viewModelScope)
    }
    init {
        getSections()
    }

}
