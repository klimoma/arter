package cz.arter.app.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.window.DialogProperties
import androidx.compose.ui.window.DialogWindowProvider
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import com.ramcosta.composedestinations.spec.DestinationStyle


@Composable
@Destination(style =  AlertDialogStyle::class)
fun AlertDialogScreen(
    navigator: DestinationsNavigator,
    dialogTitle: String,
    dialogText: String,
) {


    Box (modifier = Modifier.fillMaxSize().background(Color.Black.copy(0.5f))){
        AlertDialog(
            title = {
                Text(text = dialogTitle)
            },
            text = {
                Text(text = dialogText)
            },
            onDismissRequest = {
                navigator.popBackStack()
            },
            confirmButton = {
                TextButton(
                    onClick = {
                        navigator.popBackStack()
                    }
                ) {
                    Text("OK")
                }
            }
        )
    }



}

object AlertDialogStyle: DestinationStyle.Dialog {
    override val properties: DialogProperties
        get() = DialogProperties(
            dismissOnClickOutside = true,
            dismissOnBackPress = false,
            usePlatformDefaultWidth = false
        )

}
