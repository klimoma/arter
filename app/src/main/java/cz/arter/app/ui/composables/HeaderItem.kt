package cz.arter.app.ui.composables
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow.Companion.Ellipsis
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import cz.arter.app.model.Widget
import cz.arter.app.util.WidgetConfig

@Composable
fun HeaderItem(widget: Widget) {
    Text( widget.text?:"",style = TextStyle(fontSize = WidgetConfig.headerFontSize(widget.size?:1).sp))

}

@Preview
@Composable
fun HeaderItemPreview(){
    HeaderItem(widget = Widget(text =  "Cateringovy servis", size = 2))
}