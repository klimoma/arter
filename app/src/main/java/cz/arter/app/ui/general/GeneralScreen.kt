package cz.arter.app.ui.general

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.arter.app.model.WidgetType
import cz.arter.app.ui.composables.FileItem
import cz.arter.app.ui.composables.GalleryItem
import cz.arter.app.ui.composables.HeaderItem
import cz.arter.app.ui.composables.ImageItem
import cz.arter.app.ui.composables.ListItemItem
import cz.arter.app.ui.composables.ParagraphItem

@Composable
@Destination(
    navArgsDelegate = GeneralScreenNavArgs::class
)
fun GeneralScreen(viewModel: GeneralViewModel = hiltViewModel(), navigator: DestinationsNavigator) {
    val mainSectionState = viewModel.mainSection.collectAsStateWithLifecycle().value


    LazyColumn(modifier = Modifier.fillMaxHeight()) {
        item { Text(mainSectionState.title ?: "")}
        val widgets = mainSectionState.widgets ?: listOf()
        items(widgets){
            when (it.type) {
                WidgetType.LINK -> FileItem(it, navigator)
                WidgetType.HEADER -> HeaderItem(it)
                WidgetType.PARAGRAPH -> ParagraphItem(it)
                WidgetType.IMAGE -> ImageItem(it, navigator)
                WidgetType.GALLERY -> GalleryItem(it, navigator)
                else -> Text(text = "Neznamy prvek ${it.type}")
            }
        }

        val childern = mainSectionState.childSections ?: listOf()
        items(childern){
            ListItemItem(section = it, navigator = navigator)
        }
    }
}

data class GeneralScreenNavArgs(
    val sectionGuid: String
)