package cz.arter.app.ui.general

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cz.arter.app.model.Section
import cz.arter.app.repositories.GeneralRepository
import cz.arter.app.ui.destinations.GeneralScreenDestination
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class GeneralViewModel @Inject constructor(
    private val generalRepository: GeneralRepository,  private val savedStateHandle: SavedStateHandle
): ViewModel()  {
    private val guid: String = GeneralScreenDestination.argsFrom(savedStateHandle).sectionGuid

    val _mainSection = MutableStateFlow(Section())
    val mainSection = _mainSection.asStateFlow()

    fun fetchData() {
        generalRepository.getSectionWithChildren(guid).onEach {
            _mainSection.value = it
        }.launchIn(viewModelScope)
    }
    init {
        fetchData()
    }
}