package cz.arter.app.ui.composables

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PageSize
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.SubcomposeAsyncImage
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.arter.app.R
import cz.arter.app.model.Widget
import cz.arter.app.ui.destinations.AlertDialogScreenDestination
import cz.arter.app.ui.destinations.GalleryScreenDestination
import java.util.ArrayList

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun GalleryItem(widget: Widget, navigator: DestinationsNavigator) {
    val pagerState = rememberPagerState(pageCount = {
        widget.images?.size ?:0
    })
    HorizontalPager(state = pagerState,
        modifier = Modifier
            .fillMaxWidth()
            .height(150.dp),
        beyondBoundsPageCount = 3,
        pageSize = PageSize.Fixed(170.dp),
        contentPadding = PaddingValues(end = 64.dp),
        pageSpacing = 5.dp
        ) { page ->
        Box(  modifier = Modifier
            .fillMaxHeight()
            .clip(shape = RoundedCornerShape(8.dp, 8.dp, 8.dp, 8.dp))
            .clickable {
                widget.images?.let {

                    widget?.images?.let {
                        val  urls = ArrayList<String>()
                        urls.addAll(it)
                        navigator.navigate(GalleryScreenDestination(urls = urls))
                    }

                }

            }
        ) {

            SubcomposeAsyncImage(
                loading = { CircularProgressIndicator(modifier = Modifier.scale(0.1f)) },
                model = widget.images?.get(page),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                error = { Icon(painter = painterResource(id = R.drawable.baseline_error_24), contentDescription ="" ) },
                modifier = Modifier
                    .fillMaxSize()
            )
        }
    }

}