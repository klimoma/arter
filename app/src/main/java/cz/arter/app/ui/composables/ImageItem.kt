package cz.arter.app.ui.composables

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import coil.compose.SubcomposeAsyncImage
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.arter.app.R
import cz.arter.app.model.Widget
import cz.arter.app.ui.destinations.GalleryScreenDestination
import java.util.ArrayList


@Composable
fun ImageItem(widget: Widget, navigator: DestinationsNavigator) {

    var aspectRatio = widget.width?.let { w ->
        widget.height?.let { h ->
            w.toFloat() / h.toFloat()
        }
    }
    val urls = ArrayList<String>()
    widget.images?.firstOrNull()?.let {
        urls.add(it)
    }

    SubcomposeAsyncImage(
        loading = { CircularProgressIndicator(modifier = Modifier.scale(0.1f)) },
        model = widget.images?.firstOrNull(),
        contentDescription = null,
        contentScale = ContentScale.FillWidth,
        error = {
            Icon(
                painter = painterResource(id = R.drawable.baseline_error_24),
                contentDescription = ""
            )
        },
        modifier = if (aspectRatio != null)
            Modifier
                .fillMaxWidth()
                .aspectRatio(aspectRatio)
                .clickable { navigator.navigate(GalleryScreenDestination(urls = urls)) }
        else
            Modifier
                .fillMaxWidth()
                .clickable { navigator.navigate(GalleryScreenDestination(urls = urls)) }

    )

}