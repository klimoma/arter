package cz.arter.app.ui.composables

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.arter.app.R
import cz.arter.app.model.Section
import cz.arter.app.ui.theme.RedArter
import cz.arter.app.util.getResourceDrawable
import cz.arter.app.util.schemeNavigation
import retrofit2.http.Url

@Composable
fun ListItemItem(section: Section, navigator: DestinationsNavigator?) {
    val context = LocalContext.current
    val uriHandler = LocalUriHandler.current
    val drawableId = remember(section.imageUrl) {
        context.getResourceDrawable(section.imageUrl?:"")
    }
    val spacing = 8.dp

    Box(

        modifier = Modifier
            .fillMaxWidth()
            .clickable { navigator?.schemeNavigation(section.schemeUrl?:"", section.guid, context = context, uriHandler) }
            .padding(spacing)
        ,

    ){
        Row {

            Icon(
                painterResource(id = drawableId), section.title, tint = Color.White,
                modifier = Modifier.size(80.dp).clip(shape = RoundedCornerShape(16.dp)).background(RedArter)
            )
            Text(
                text = section.title ?: "",
                textAlign = TextAlign.Left,
                color = Color.Black,
                modifier = Modifier
                    .weight(1f)
                    .align(Alignment.CenterVertically).padding(horizontal = spacing)
            )
            Icon(
                Icons.Default.KeyboardArrowRight, "dalsi", tint = Color.Gray,
                modifier = Modifier.align(Alignment.CenterVertically)
            )
        }
    }
}

