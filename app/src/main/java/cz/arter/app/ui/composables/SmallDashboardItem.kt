package cz.arter.app.ui.composables

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.arter.app.model.Section
import cz.arter.app.util.schemeNavigation

@Composable
fun SmallDashboardItem(section: Section, navigator: DestinationsNavigator?) {
    val context = LocalContext.current
    val uriHandler = LocalUriHandler.current
    OutlinedButton(
        shape = RoundedCornerShape(12.dp),
        onClick = {navigator?.schemeNavigation(section.schemeUrl?:"", section.guid,context, uriHandler)},
        contentPadding = PaddingValues(8.dp),
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .padding(vertical = 8.dp)
        ,
        border = BorderStroke(0.dp, Color.Gray),
        colors = ButtonDefaults.buttonColors(Color.Gray)

    ){
        Row {


            Text(
                text = section.title ?: "",
                textAlign = TextAlign.Left,
                modifier = Modifier
                    .weight(1f)
                    .align(Alignment.CenterVertically)
            )
            Icon(
                Icons.Default.KeyboardArrowLeft, "dalsi", tint = Color.White,
                modifier = Modifier
            )
        }
    }
}




@Preview
@Composable
fun SmallDashboardPreview() {
    SmallDashboardItem(Section(title = "dadas").apply {
        childSections = listOf(Section(title = "child"))
    }, null)
}