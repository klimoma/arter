package cz.arter.app.ui.composables

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.arter.app.model.Section
import cz.arter.app.ui.theme.GreenRicany
import cz.arter.app.util.schemeNavigation

@Composable
fun BigDashboardItem(section: Section, navigator: DestinationsNavigator?, backgroundColor: Color) {
    val context = LocalContext.current
    val uriHandler = LocalUriHandler.current
    OutlinedButton(
        shape = RoundedCornerShape(12.dp),
        onClick = {navigator?.schemeNavigation(section.schemeUrl?:"", section.guid, context = context, uriHandler)},
        contentPadding = PaddingValues(8.dp),
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
            .padding(vertical = 8.dp)
        ,
        border = BorderStroke(0.dp, backgroundColor),
        colors = ButtonDefaults.buttonColors(backgroundColor)

    ) {
        Row {
            Column(modifier = Modifier.weight(1f)) {

                Text(
                    text = section.title ?: "",
                    modifier = Modifier

                )
            }

            Column(modifier = Modifier.weight(1f),

                )  {

                section.childSections?.forEach {
                    OutlinedButton(
                        shape = RoundedCornerShape(12.dp),
                        onClick = {navigator?.schemeNavigation(section.schemeUrl?:"", it.guid, context, uriHandler)},
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 8.dp),
                        colors = ButtonDefaults.outlinedButtonColors(contentColor = Color.White),
                        border = BorderStroke(1.dp, Color.White),
                    ){

                        Text(
                            text = "${it.title ?: ""}")
                    }
                }

            }
        }
    }

}



@Preview
@Composable
fun BigDashboardPreview() {
    BigDashboardItem(section = Section(title = "dadas").apply {
        childSections = listOf(Section(title = "child"))
    }, null, GreenRicany)
}
