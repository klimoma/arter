package cz.arter.app.ui.composables

import android.text.util.Linkify
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.text.HtmlCompat
import com.google.android.material.textview.MaterialTextView
import cz.arter.app.model.Widget
import cz.arter.app.util.WidgetConfig
import cz.arter.app.util.WidgetConfig.regularFontSize

@Composable
fun ParagraphItem(widget: Widget) {
    val spannedText = HtmlCompat.fromHtml(widget.text?:"", 0)
   // Text( spannedText,style = TextStyle(fontSize = regularFontSize.sp))
    AndroidView(
      //  modifier = modifier,
        factory = {
            MaterialTextView(it).apply {
                // links
                autoLinkMask = Linkify.WEB_URLS
                linksClickable = true
               // link
                // setting the color to use forr highlihting the links
                setLinkTextColor(Color.White.toArgb())
            }
        },
        update = {
           // it.maxLines = currentMaxLines
            it.text = spannedText
        }
    )
}



@Preview
@Composable
fun ParagraphItemPreview(){
    ParagraphItem(widget = Widget(text =  "preview text", size = 1))
}