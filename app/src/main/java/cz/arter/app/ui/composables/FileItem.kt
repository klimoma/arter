package cz.arter.app.ui.composables

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowLeft
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.arter.app.model.Section
import cz.arter.app.model.Widget
import cz.arter.app.util.schemeNavigation
import dagger.hilt.android.qualifiers.ActivityContext

@Composable
fun FileItem(widget: Widget, navigator: DestinationsNavigator?) {
    val uriHandler = LocalUriHandler.current
    val context = LocalContext.current
    OutlinedButton(
    shape = RoundedCornerShape(12.dp),
    onClick = {navigator?.schemeNavigation(widget.url?:"",context = context, uriHandler = uriHandler )},
    contentPadding = PaddingValues(6.dp),
    modifier = Modifier
    .fillMaxWidth()
    .padding(horizontal = 16.dp)
    .padding(vertical = 8.dp)
    ,
    border = BorderStroke(0.dp, Color.Gray),
    colors = ButtonDefaults.buttonColors(Color.Gray)

    ){
        Row {
            Text(
                text = widget.text ?: "",
                textAlign = TextAlign.Left,
                modifier = Modifier
                    .weight(1f)
                    .align(Alignment.CenterVertically)
            )
            Icon(
                Icons.Default.KeyboardArrowLeft, "dalsi", tint = Color.White,
                modifier = Modifier
            )
        }
    }
}