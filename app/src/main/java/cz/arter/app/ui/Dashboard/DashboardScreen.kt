package cz.arter.app.ui.Dashboard

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.ramcosta.composedestinations.annotation.Destination
import com.ramcosta.composedestinations.navigation.DestinationsNavigator
import cz.arter.app.model.SectionType
import cz.arter.app.ui.composables.BigDashboardItem
import cz.arter.app.ui.composables.SmallDashboardItem
import cz.arter.app.ui.theme.BluePraha11
import cz.arter.app.ui.theme.GreenRicany

@Composable
@Destination
fun DashboardScreen(viewModel: DashboardViewModel = hiltViewModel(), navigator: DestinationsNavigator) {
    val dashboardSections = viewModel.dashboardSection.collectAsStateWithLifecycle().value

    LazyColumn(modifier = Modifier.fillMaxHeight()) {
        items(dashboardSections.size, key = { dashboardSections[it].guid?:""} ){
            val item = dashboardSections[it]
            if (item.type == SectionType.BIG_DASHBOARD_TILE) {
                //todo tohle chce doresit na nejaky type nebo option
                val color = if(item.title?.contains("Praha") == true) BluePraha11 else GreenRicany
                BigDashboardItem(section = item, navigator, color)
            } else {
                SmallDashboardItem(item, navigator)
            }
        }


    }


}
