package cz.arter.app.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import cz.arter.app.model.GeneralResponseDto
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.http.GET
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

// from https://gitlab.com/restcountries/restcountries
interface GeneralApi {
    @GET("database.json")
    suspend fun getAll(): GeneralResponseDto
}

//
@Module
@InstallIn(SingletonComponent::class)
/*abstract*/ object NetworkModule {
// ale tady je abstract https://developer.android.com/training/dependency-injection/hilt-android

    //    @Binds abstract fun bindAnalyticsService(analyticsServiceImpl: AnalyticsServiceImpl): AnalyticsService
    private const val BASE_URL = "https://preparator.eternal.cz/data/arter/"
//    @Provides
//    @Singleton
//    fun provideGeneralApi(): GeneralApi {
//        val json = Json { ignoreUnknownKeys = true }
//        return Retrofit.Builder()
//            .addConverterFactory(json.asConverterFactory("application/json".toMediaType()))
//            .baseUrl(BASE_URL)
//            .build().create(GeneralApi::class.java)
//    }

    @Singleton
    @Provides
    fun provideGeneralApi(retrofit: Retrofit): GeneralApi =
        retrofit.create(GeneralApi::class.java)

    @Singleton
    @Provides
    fun provideHttpClient(/*networkFlipperPlugin: NetworkFlipperPlugin*/): OkHttpClient {
        return OkHttpClient
            .Builder()
           // .addNetworkInterceptor(FlipperOkhttpInterceptor(networkFlipperPlugin))
            .readTimeout(15, TimeUnit.SECONDS)
            .connectTimeout(15, TimeUnit.SECONDS)
            .build()
    }

//    @Singleton
//    @Provides
//    fun flipperNetworkPlugin() = NetworkFlipperPlugin()

    @Singleton
    @Provides
    fun provideConverterFactory(): Converter.Factory  {
        val json = Json {
            encodeDefaults = false
            ignoreUnknownKeys = true
        }
        return json.asConverterFactory("application/json".toMediaType())
    }




    @Singleton
    @Provides
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        converterFactory: Converter.Factory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(converterFactory)
            .build()
    }
}