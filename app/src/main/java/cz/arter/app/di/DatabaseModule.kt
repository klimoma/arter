package cz.arter.app.di

import android.content.Context
import androidx.room.Room
import cz.arter.app.local.database.AppDatabase
import cz.arter.app.local.database.SectionDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Provides
    fun provideSectionDao(appDatabase: AppDatabase): SectionDao {
        return appDatabase.sectionDao()
    }

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            "Arter"
        ).fallbackToDestructiveMigration().build()
    }
}