package cz.arter.app.repositories


import cz.arter.app.di.GeneralApi
import cz.arter.app.local.database.SectionDao
import cz.arter.app.model.Section
import cz.arter.app.model.SectionType
import cz.arter.app.model.toSection
import cz.arter.app.model.toSectionEntity
import cz.arter.app.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject


interface GeneralRepository {
    fun getAllSections(): Flow<Resource<List<Section>>>

    fun getDashboardSection(): Flow<List<Section>>

    fun getSectionByGuidsWithLimit(guids: List<String>): Flow<List<Section>>

    fun getSectionWithChildren(guid: String): Flow<Section>
}

class GeneralRepositoryImpl @Inject constructor(
    private val api: GeneralApi, private val sectionDao: SectionDao
) : GeneralRepository {

    override fun getAllSections(): Flow<Resource<List<Section>>> = flow {
        emit(Resource.Loading())

        val sections = listOf<Section>()//sectionDao.getAll().map { it.toSection() }
        emit(Resource.Loading(data = sections))


        try {
            val remoteSections = api.getAll()
            //  todo prace s db dodelat

            sectionDao.insertAll(remoteSections.sections?.map { it.toSectionEntity() } ?: listOf())
            //  dao.insertWordInfos(remoteWordInfos.map { it.toWordInfoEntity() })
            emit(Resource.Success(data = remoteSections.sections?.map { it.toSection() }))
        } catch (e: HttpException) {
            emit(
                Resource.Error(
                    message = "Neco se nepovedlo",
                    data = sections
                )
            )
        } catch (e: IOException) {
            emit(
                Resource.Error(
                    message = "Nejde se připojit k serveru, prosím zkontrolujte připojení k internetu",
                    data = sections
                )
            )
        }


    }

    override fun getDashboardSection(): Flow<List<Section>> {
        return flow {
            val sections = sectionDao.getDashboardSections().map { it.toSection() }

            sections.forEach { section ->
                if (section.type == SectionType.BIG_DASHBOARD_TILE)
                    section.childs?.let {
                        section.childSections =
                            sectionDao.getSectionsByGuidsWithLimit(it)?.map { it.toSection() }
                    }

            }

            emit(sections)
        }
    }

    override fun getSectionByGuidsWithLimit(guids: List<String>): Flow<List<Section>> {
        return flow {
            emit(sectionDao.getSectionsByGuidsWithLimit(guids).map { it.toSection() })
        }
    }

    override fun getSectionWithChildren(guid: String): Flow<Section> {
        return flow {
            val mainSection = sectionDao.getSectionsByGuid(guid).toSection()
            val childrenSection = sectionDao.getSectionsByGuids(mainSection.childs?: listOf()).map { it.toSection() }
            mainSection.childSections = childrenSection
            emit(mainSection)
        }
    }

}

//interface GeneralRepository {
//    val countrys: Flow<List<String>>
//
//    suspend fun add(name: String)
//}
//
//class DefaultGeneralRepository @Inject constructor(
//    private val countryDao: SectionDao
//) : GeneralRepository {
//
//    override val countrys: Flow<List<String>> =
//        countryDao.getCountrys().map { items -> items.map { it.name } }
//
//    override suspend fun add(name: String) {
//        countryDao.insertCountry(Country(name = name))
//    }
//}